import { useState } from "react";
import Button from "./components/Button/Button";
import ModalText from "./components/Modal/ModalText/ModalText";
import ModalImage from "./components/Modal/ModalImage/ModalImage";

function App() {
  const [isFirstModalOpen, setIsFirstModalOpen] = useState(false);
  const [isSecondModalOpen, setIsSecondModalOpen] = useState(false);

  return (
    <>
      <Button onClick={() => setIsFirstModalOpen(true)}>
        Open first modal
      </Button>
      <Button onClick={() => setIsSecondModalOpen(true)}>
        Open second modal
      </Button>

      {isFirstModalOpen && (
        <ModalImage onClose={() => setIsFirstModalOpen(false)} />
      )}
      {isSecondModalOpen && (
        <ModalText onClose={() => setIsSecondModalOpen(false)} />
      )}
    </>
  );

}

export default App;
