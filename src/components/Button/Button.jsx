import styled from "styled-components";

const StyledButton = styled.button`
  background: fff;
  border-radius: 8px;
  border: 1px solid;
  color: black;
  padding: 15px;
  font-size: 16px;

  &:hover {
    background: darkviolet;
    cursor: pointer;
  }
`;

function Button({ children, type = "button", onClick }) {
  return (
    <StyledButton type={type} onClick={onClick}>
      {children}
    </StyledButton>
  );
}

export default Button;
