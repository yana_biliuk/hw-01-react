import styled from "styled-components";
import ModalWrapper from "./ModalWrapper/ModalWrapper";



const Container = styled.div`
padding: 56px 56px;
border-radius: 16px;
background: #FFF;
box-shadow: 0px 4px 4px 0px rgba(0, 0, 0, 0.25);
position: relative;

`


function Modal({children, onClose }) {

   
    return (
        <ModalWrapper onClose={onClose} > 
        <Container>
        {children}
        </Container>
            </ModalWrapper>
        
    )
    
}


export default Modal
