import Modal from "../Modal";
import ModalClose from "../ModalClose/ModalClose";
import ModalHeader from "../ModalHeader/ModalHeader ";
import ModalBody from "../ModalBody/ModalBody";
import ModalFooter from "../ModalFooter/ModalFooter";

function ModalText({ onClose }) {
  return (
    <Modal onClose={onClose}>
      <ModalClose onClick={onClose} />
      <ModalHeader>Add Product 'NAME'</ModalHeader>
      <ModalBody>
        <p>Description for you product</p>
      </ModalBody>
      <ModalFooter firstText="ADD TO FAVORITE" firstClick={onClose} />
    </Modal>
  );
}
export default ModalText;
