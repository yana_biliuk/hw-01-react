import styled from "styled-components";

const TextHeader = styled.div`
  display: flex;
  justify-content: center;
  font-size: 32px;
  font-weight: 400;
`;

function ModalHeader({ children }) {
  return <TextHeader>{children}</TextHeader>;
}

export default ModalHeader;
