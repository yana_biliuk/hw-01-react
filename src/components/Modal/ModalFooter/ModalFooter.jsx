import Button from "../../Button/Button";
import styled from "styled-components";

const Footer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 64px;
  gap: 64px;
`;

function ModalFooter({ firstText, secondaryText, firstClick, secondaryClick }) {
  return (
    <Footer>
      {firstText && <Button onClick={firstClick}>{firstText}</Button>}
      {secondaryText && (
        <Button onClick={secondaryClick}>{secondaryText}</Button>
      )}
    </Footer>
  );
}

export default ModalFooter;
