import styled from "styled-components";

const CloseModal = styled.button`
  position: absolute;
  top: 10px;
  right: 10px;
  background: none;
  border: none;
  font-size: 24px;
  cursor: pointer;
`;

function ModalClose({ onClick }) {
  return <CloseModal onClick={onClick}>&times;</CloseModal>;
}

export default ModalClose;
